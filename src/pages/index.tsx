import React, { useEffect, useState } from "react";
import { Inter } from "next/font/google";
import dynamic from "next/dynamic";

// const SimpleGraph = React.lazy(() => import("@/components/SimpleGraph"));
const SimpleGraph = dynamic(() => import("@/components/SimpleGraph"), {
  ssr: false,
  loading: () => <p>Loading...</p>,
});

const inter = Inter({ subsets: ["latin"] });

export default function Home() {
  const [isLoaded, setIsLoaded] = useState(false);

  useEffect(() => {
    // Client-side-only code
    setIsLoaded(true);
  });

  return (
    <>
      <h1>Graph Project</h1>
      <p>This is a simple graph</p>
      {(isLoaded && <SimpleGraph />) || "Loading..."}
    </>
  );
}
