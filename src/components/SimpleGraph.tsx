// IMPORTS
import React from "react";
import { ForceGraph3D } from "react-force-graph";

// DATA
const numNodes = 30;
// const nodeTree = { nodes: [], links: [] };
const nodeTree = {
  nodes: [...Array(numNodes).keys()].map((i) => ({ id: i })),
  links: [...Array(numNodes).keys()]
    .filter((id) => id)
    .map((id) => ({
      source: id,
      target: Math.round(Math.random() * (id - 1)),
    })),
};

// RENDER
export default function SimpleGraph() {
  return (
    <ForceGraph3D
      backgroundColor={"rgba(0,0,0,0)"}
      nodeColor={() => "yellow"}
      linkColor={() => "white"}
      graphData={nodeTree}
    />
  );
}
